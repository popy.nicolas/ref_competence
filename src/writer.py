import sqlite3


def hard_cache(function):
    cache = {}

    def wrapper(*args, id=None, **kwargs):
        if not id in kwargs:
            data_hash = hash(tuple(v for k, v in sorted(kwargs.items())))
        else:
            data_hash = kwargs[id]

        if args[1] in cache:
            if data_hash in cache[args[1]]:
                return cache[args[1]][data_hash]
        else:
            cache[args[1]] = dict()

        result = function(*args, id=id, **kwargs)
        cache[args[1]].update({data_hash: result})
        return result

    return wrapper


class SQLiteWriter:
    connection = None

    def __enter__(self):

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.execute_insert_batch("npec", self.batch)
        self.cursor.close()
        SQLiteWriter.connection.commit()
        SQLiteWriter.connection.close()

    def __init__(self, path):
        if not SQLiteWriter.connection:
            SQLiteWriter.connection = sqlite3.connect(path)

        self.cursor = SQLiteWriter.connection.cursor()

        self.batch = []
        self.pks = {}

    @hard_cache
    def execute_insert(self, table, id=None, **data):
        # python 3.7 keys() on dictonary return them in insertion order
        fields_str = ", ".join(f for f in data.keys())
        values_str = ", ".join(f":{f}" for f in data.keys())
        query = f"INSERT INTO {table}({fields_str}) values ({values_str});"

        self.cursor.execute(
            query, data)
        if not id in data:
            return self.cursor.lastrowid
        return data[id]

    def execute_insert_batch(self, table, batch):
        fields_str = ", ".join(f for f in batch[0].keys())
        values_str = ", ".join(f":{f}" for f in batch[0].keys())
        query = f"INSERT INTO {table}({fields_str}) values ({values_str});"
        self.cursor.executemany(
            query, batch)

    def create_db(self):
        with open("scripts/init.sql", "r") as f:
            sql_script = f.read()
        self.cursor.executescript(sql_script)
        SQLiteWriter.connection.commit()

    def write(self, batch: list):
        for data in batch:
            if not self.pks:
                self.pks = {"diplome_id": self.execute_insert("diplome", id="diplome_id",
                                                              label=data.pop("libelle_du_diplome")),
                            "certificateur_id": self.execute_insert("certificateur", id="certificateur_id",
                                                                    label=data.pop("certificateur")),
                            "formation_id": self.execute_insert("formation", id="formation_id",
                                                                label=data.pop("libelle_de_la_formation"))}

                self.pks["rncp_id"] = self.execute_insert("rncp", id="rncp_id", diplome_id=self.pks["diplome_id"],
                                                          certificateur_id=self.pks["certificateur_id"],
                                                          formation_id=self.pks["formation_id"],
                                                          code=data.pop("code_rncp"))

            self.batch.append(
                {
                    "npec_final": data.get("npec_final"),
                    "status": data.get("statut"),
                    "date": data.get("date_dapplicabilite_des_npec"),
                    "rncp_id": self.pks["rncp_id"],
                    "code_cpne": self.execute_insert("cpne", id="code_cpne", code_cpne=data.get("code_cpne"),
                                                     label=data.get("cpne"))
                })
        self.pks = {}
