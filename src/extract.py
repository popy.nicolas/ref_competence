import argparse

import reader
import writer


def main():
    parser = argparse.ArgumentParser(
        description="parse a file from :"
    )
    parser.add_argument(
        "--file",
        type=str
    )
    args = parser.parse_args()
    if not args.file:
        parser.print_help()
        exit()


    with reader.XLReader(args.file, 'Onglet 3 - référentiel NPEC', 3) as xlr:
        with writer.SQLiteWriter("data/rncpe_acronym.db") as w:
            w.create_db()
            for batch in xlr.get_batch():
                w.write(batch)


if __name__ == '__main__':
    main()
