# referentiels france competence

Il s'agit de mettre a disposition les donnees du referentiels france competence de
maniere a rendre facile et efficace la recherche dans ces donnees.

## Installation
```bash
./setup.sh
./extract.sh --file <path_to_file>
```

## lien vers les donnees:

Les referentiels peuvent etre telecharger sur la page suivante.

https://www.francecompetences.fr/base-documentaire/referentiels-et-bases-de-donnees/

